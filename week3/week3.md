## **In week 3 Hashem explained to us the different types of laser machines by presentation and we started working on different kinds of laser cut machines. Each one of them has a different propose and use.**

### Some of the laser cut machines we worked on are as follow:
### 1.	Engraving
### 2.	2D Cut
### 3.	Pressfit Kits
### We started learning how to cut by using laser, the speed of the cut, and the power we use in cutting the objects. They taught us on how we can manage the machine and how to operate it on different kinds of material and the shapes we can make on the settings.

### Engraving started with mw using the laser machine on a small square shaped piece of MDF that has a thickness of 3 mm. I engraved my name on five pieces of MDF and numbering each one starting with one and ending with five. Each one has a different laser power and speed in cutting the pieces of MDF.

### We can use the laser machine on many different kinds of material and in my assignment I used small pieces of MDF to show the power and speed of the laser as it is required in the assignment.

### I worked on making my own sticker on a different machine that works by designing the sticker on an application and after it uploading it onto the machine. I chose the Jeep logo design from the internet and converted through the application to get my sticker done. I printed the sticker and applied the protective plastic on the sticker. These steps were taught by our instructor to print a sticker the way we designed and having it saved and ready to be used in an easy way.

### After my instructor explained to us the laser cut machine, I worked on my first test which is creating an acrylic piece. I chose the acrylic piece and its thickness which is 6 millimetre in thickness and started cutting it in an oval shape. After that I engraved my writing ‘’ali-web’’ on the piece.
![1](images/step2-2.png)

### I tried on the Universal Laser System machine onto a piece of MDF 3 mm in thickness and cut the MDF into twelve equal pieces. The kerf of the MDF is found by taking 60 mm 56.81 then divided by 12 pieces and the kerf is equal to 0.266 mm.
### and the best cut is power 100% speed 5% PPI 500

![1](images/step3.png)
### I used fusion 360 app to make the shape for press fit pieces by drawing a square shape and making the slot height for it. I had a problem so I texted my classmates on the Slack app to ask for help. One of them replied and gave me the solution by making 4 slot heights at a time. It worked for me and I finished the shape.

![1](images/step5.png)

### I used the laser cut machine to make a press fit that would fit onto each other by clipping them together. I made 9 circle pieces and 8 square pieces from a MDF. After I found the kerf I took the number 3mm minus it with the kerf (0.266mm) and the result is 2.734 mm. This way the MDF pieces will press fit into each other.

![1](images/step4.jpg)
![1](images/step6.jpg)

## **I started working on my sticker and I chose the official Jeep logo for my assignment. There are many programs that we can use to make stickers but I chose Corel Draw as my program since I know how to use it. To start making my sticker I need to start adjusting the sticker printer. The printer I am using is called Roland CAMM-1 GS-24. I placed the vinyl roll onto the printer and made sure the roll is over the sensors to print. I placed the roll straight onto the lines shown on the printer but the piece of the roll was short in width so I couldn’t chose my setting to print the sticker. I threw away the piece of vinyl and took a larger piece and switched on the machine and it is ready for the command.**

![1](images/sticker1-11.jpg)

(https://favpng.com/png_view/jeep-vector-logo-jeep-wrangler-car-jeep-cj-logo-png/mWte39rn)
