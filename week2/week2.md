
# **FabTinyISP Minimal**

### The "FabTinyISP Minimal" is what we are going to build, an AVR programmer and also it needs an AVR programmer to build it. There are steps to follow for building it and use it later for programming.  There are some commonents to obtain so you can build you FabtinyISP Minimal.  The FabTinyISP is a "low-speed" USB 1.1 device it is one of the slowset of the USB. We will start cutting and building the USB from the beggining. The first steps will be cutting the boards ansd shaping it after that programming it and prepare the settings and make sure it is programmed as needed.
## _Traces:-_

### Enter the Fab website and save the image from the link below Traces (1000 dpi) than you can use the image for the cutting program.

## _Outline:-_

### Enter the Fab website and save the image from the link below Traces (1000 dpi) than you can use the image for the cutting program.

![1](images/step111.jpg)

### Enter the Fabmodules.org website to start entering the machine settings.
### There will be steps to follow to input the machine settings.
### 1.	In the input format chose ‘’image (.png)’’
### 2.	For the output format chose ‘’Roland mill (.rm1)’’
### 3.	In the process chose ‘’PCB traces (1/64)’’
### 4.	Than in the output machine chose ‘’SRM-20’’ than change the X,Y &Z To:-
### a.	X0 (mm):0
### b.	Y0 (mm):0
### c.	Z0 (mm):0
### 5.	In the process than click on calculate and wait to see the changes after that you press on ‘’Save’’ to save the settings you did.

![1](images/step222.jpg)

### Enter the Fabmodules.org website to start entering the machine settings.
### There will be steps to follow to input the machine settings.
### 1.	In the input format chose ‘’image (.png)’’
### 2.	For the output format chose ‘’Roland mill (.rm1)’’
### 3.	In the process chose ‘’PCB traces (1/32)’’
### 4.	Than in the output machine chose ‘’SRM-20’’ than change the X,Y &Z To:-
### a.	X0 (mm):0
### b.	Y0 (mm):0
### c.	Z0 (mm):0
#### 5.	In the process than click on calculate and wait to see the changes after that you press on ‘’Save’’ to save the settings you did.

![1](images/step3-3.jpg)

### This is the program for the cutting machine you start by placing the PCB board in the cutting machine than you rest the machine settings to a new settings:-
### Move the cutting machine to reach the new starting point. Press X/Y to rest the settings when you place the milling bit in the starting point you want than it will be rest when the X and Y will show zero. The Z point you need to rest it manually by placing the milling bit in the point you want and press Z to show zero than it is ready to start cutting:-

### Press CUT to start cutting.

![1](images/step4.jpg)

### After clicking on CUT a new window will show:-
### -	First delete the old settings by clicking on delete all after click on add to download my settings that I made before. My settings will show the points I wanted to cut, all the points will be cut on the PCB board.
### -	Press Output to start milling the PCB board.

![1](images/step5.jpg)

![1](images/step6.jpg)

## Components to obtain

![1](images/step7.jpg)

### The PCB board after cutting it and soldering the resisters.

![1](images/step8.jpg)

## _**After I finished making the PCB board, I programmed the board by using the software that I have installed. The steps I made for programming are below.**_

### I installed Atmel AVR Toolchain for Windows on my computer and I extracted the ZIP file. I placed the folder inside the ‘’Program Files’’ on my hard disk.

![1](images/step10.jpg)

![1](images/step11.jpg)

## I already installed Git from before on my computer.

![1](images/step12.jpg)

## I installed GNU make on my computer and accepted the location for installation that is in Downloads.

![1](images/step13.jpg)

## I downloaded AVR dude and I unzip it on my hard disk inside the ‘’Program Files’’.

![1](images/step14.jpg)

## I opened the hard disk folder, then Program Files, then AVR Toolchain folder, then bin folder, and after it I copied the link of the folder to use it in the ‘’Environmental Variables’’ for programming.

![1](images/step15.jpg)

## I opened the hard disk folder, then Program Files, the avrdude, and after it I copied the link of the folder to use it in the ‘’Environmental Variables’’ for programming.

![1](images/step16.jpg)

## I opened the hard disk folder, Program Files x86 (in Arabic), GnuWin32 folder, bin folder, and after it I copied the link to use it in the ‘’Environmental Variables’’ for programming.

![1](images/step17.jpg)

## I opened the control panel, then System and Security, then System, then Advanced System Setting, then I clicked on Environmental Variables and chose Path and added the 3 links I copied from before that are shown in the previous photos into the window and accepted.

![1](images/step1818.jpg)

## I downloaded Zadig located inside the Downloads file and launched it, then I chose the WinUSB and installed the driver onto it.

![1](images/step18.jpg)

## I opened my file ali-website and right clicked in the file to start using the Git Bash software.

![1](images/step19.jpg)

## I typed ‘’make –v’’ then pressed enter to find the result ‘’GNU Make 3.81’’ as shown in the picture.
## I then typed ‘’avr-gcc –version’’ to find the result ‘’avr-gcc.exe (AVR_8_bit_GNU_Toolchain_3.6.2_1759) 5.4.0’’
## I also typed ‘’arvdude –c usbtiny –p t45’’ but I didn’t find a result on the code.

![1](images/step20.jpg)

## I downloaded ‘’firmware source code’’ from the link onto my computer and unzip the folder on my desktop.

![1](images/step21.jpg)

## I opened Git Bash and entered ‘’make fuses’’ with the result being an application error as shown  when I used it on my computer.

![1](images/step22.jpg)

## I used another laptop and tried the same process with the ‘’make fuses’’ code on the Git Bash and the result worked as shown above.

![1](images/step23.jpg)

## I opened Git Bash and typed ‘’make flash’’ and the result is shown  in the picture.

![1](images/step24.jpg)

## I opened Git Bash and typed ‘’make rstdisbl’’ and the result is shown above in the picture.

![1](images/step25.jpg)
