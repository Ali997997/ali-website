# **Portable Life streaming case**
# **For covering fire accident**
### Fablab UAE 2020





# **Introduction :**

### Based on the problems we faced in the fire accidents. The idea of the portable life streaming case was the best solution for life streaming the accidents. Reaching to a places where the cameras on the vehicle can’t reach. The commanders can follow the accidents and give orders from the central operating room.



# **Initial Sketch**

![life straming case sketch](images/initial_sketch.jpg)


# _**Things we will buy:**_     
## 1-_**camera**_  2-_**modem**_  3-_**battery**_ 4-_**Hard case**_

# _**Things we will fabricate in Fablab UAE :**_
## 1- _**3d printer**_ : Brackets and the Foundations.
## 2-laser cut: acrylic cutting
## 3-Arduino : input output and programing
## 4-molding : battery mold.

# **Applications :**

## We can use the portable life stream case for covering the accidents from all sides.
## The commanders will have full view on the accidents to run and give the right orders.
## The video will be taking form the accidents will also help in investigations after the accidents.
## It helps the trainers before going to the field and facing a real accident.
## Gives accurate and real time intelligence as to the progression of the fire spread.
# **Conclusion :**
## The idea of the portable life streaming case was to make it easy for the crew in the accident to move while they are caring the case and to ensure accident is covered from all sides easily.  

# _This is my idea which I will be working on in FABLAB academy in 2020 ._
## 1.Sing in to the git lab account
## 2.	Chose new project.

![1](images/sing_in.jpg)

## 3.	Fill the gaps.
### _A.Project name: Ali-website._
### _B.Project description: all my FabLab files will be saved in this file._
### _C. Visible Level: public._

## 4.	Create project.

![1](images/sing_in1.jpg)  

## 5. Click on Ali website

![ali](images/ali_web.jpg)

## 6. Click on clone after copy the clone with SSH.

![ssh](images/ssh.jpg)

## 7. Right click on the desktop after chose Get Bash. _I did chose desktop because it is easy to find the file_

![rightclick](images/right_click.jpg)

## 8. New window will open. Ali website user than write git clone after past the SSH you did copy.

![rightclick1](images/right_click1.jpg)

## 9. Ali website file will appear on the desktop after you press ‘’Enter’’.

## 10. Open the file. Press right click and chose gitbash. The window will open.

![window](images/window.jpg)

## 11. To add any file in the folder there are 3 steps to follow. Write in the window:
```
```
 ```git add .``` than press _‘’Enter’’_

``` git comment-m``` ‘’New commit’’ than press _‘’Enter’’_

``` git push``` than press _‘’Enter’’_
