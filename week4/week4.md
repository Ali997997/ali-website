### For this assignment I have to work on two programs that were told to us by Fab Lab for us to work on. The first program is called Eagle and I have to download this software from their official site that was given to us by Hashim during our lecture.
![1](images/step7-7.jpg)
I designed this circuit board using the Atiny44 model that was given to us. I added the parts needed on the board as shown above using the Eagle app, and the parts will be listed here below:
1-Attiny44
2-2 R1206FAB Resistor
3-1 C1206FAB Capacitor
4-8 MHz Resonator  
5-LEDFAB1206
6-CONN_03X2_AVRISPSMD
7-CONN_06_FTDI-SMD-HEADER
8-6MM_SWITCH


![1](images/step2.2.jpg)
## I opened ‘’Layer Settings’’ and pressed on Ctrl + A on all the layers and clicked on ‘’Hide Layers’’. After it I clicked on the ‘’Top’’ layer to make it visible on the image and then I clicked ok.
![1](images/step3.3.jpg)

## After I finished editing the picture I clicked on ‘’File’’ then ‘’Export’’ and I chose ‘’Image’’.

![1](images/step4.4.jpg)

## I selected ‘’Browse’’ and clicked on ‘’Week 3’’ then ‘’Images’’ and I named the picture ‘’test2’’.

![1](images/step5.5.jpg)

## I exported the picture and clicked on the ‘’Monochrome’’ box and changed the resolution to 2000 dpi and after it I saved the image in ‘’Week 3’’.

![1](images/step6.6.jpg)

![1](images/step8.8.jpg)

### I right clicked on the image and clicked on ‘’Edit using Photos’’ and then I cropped the image and saved it as it is.

![1](images/step9.9.jpg)

I right clicked the image and clicked on ‘’Open With’’ and then I chose ‘’Paint’’

![1](images/step10-10.jpg)

I opened ‘’Paint’’ and clicked on ‘’Change Shape’’ and opened ‘’Stretch and Skew’’. I changed the shape by clicking on ‘’Pixel’’ and then on ‘’OK’’.

![1](images/step11.11.jpg)

I clicked on ‘’File’’ and then on ‘’Save’’ as and I chose ‘’Image PNG’’.

![1](images/step12.12.jpg)

I clicked on ‘’Week 3’’ and then I named the image ‘’traces-ali’’ and saved it as ‘’PNG’’.

![1](images/step13.13.jpg)

I opened the ‘’PNG’’ image on ‘’Paint’’ then I clicked on ‘’Fill’’ and chose the white color. Then I used the ‘’Rectangle’’ shape on the image to make a white box over it.

![1](images/step14.14.jpg)

I clicked on ‘’File’’ and then on ‘’Save As’’ and saved the image as ‘’PNG’’. I saved in Week 3 as ‘’outline-ali’’.

![1](images/step15.15.jpg)

This is the final product of the board that I worked on for this assignment.

![1](images/step16.16.jpg)

I have used the multi meter to check the circuit if it’s working or not. I have tried to check the circuit from the micro arm BB2 to resonator and it is linked to each other, and I have checked on all of the other pin on the board they are all working properly.
